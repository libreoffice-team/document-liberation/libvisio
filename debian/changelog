libvisio (0.1.7-1) unstable; urgency=medium

  * New upstream version 0.1.7

 -- Rene Engelhard <rene@debian.org>  Sun, 18 Aug 2019 22:48:54 +0200

libvisio (0.1.6-1) unstable; urgency=medium

  * New upstream version 0.1.6

  * build with -DUCHAR_TYPE=uint16_t with ICU 59

 -- Rene Engelhard <rene@debian.org>  Sat, 21 Oct 2017 18:26:16 +0200

libvisio (0.1.5-4) unstable; urgency=medium

  * Add libicu-dev, libxml2-dev, zlib1-dev to libvisio-dev's Depends field,
    thanks ﺄﺤﻣﺩ ﺎﻠﻤﺤﻣﻭﺪﻳ (Ahmed El-Mahmoudy) for the patch
    (Closes: #758812)

 -- Rene Engelhard <rene@debian.org>  Mon, 26 Sep 2016 20:38:51 +0200

libvisio (0.1.5-3) unstable; urgency=medium

  * [0d35f52] move to Debian LibreOffice Maintainers

 -- Rene Engelhard <rene@debian.org>  Tue, 13 Sep 2016 05:27:40 +0200

libvisio (0.1.5-2) unstable; urgency=medium

  * [9ebbb7e] Build-Depend on tzdata (closes: #837017)
  * [872e04e] multiarchify
  * [8c4a718] bump dh compat to 10
  * [93a2727] fix typo in package description

 -- Rene Engelhard <rene@debian.org>  Mon, 12 Sep 2016 22:09:39 +0200

libvisio (0.1.5-1) unstable; urgency=medium

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Wed, 30 Dec 2015 10:02:13 +0100

libvisio (0.1.4-1) unstable; urgency=medium

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Fri, 25 Dec 2015 10:43:20 +0100

libvisio (0.1.3-2) unstable; urgency=medium

  * backport upstream patch to fix the tests. bump build-dep to
    libxml2 (>= 2.9.2) as it's required

 -- Rene Engelhard <rene@debian.org>  Tue, 22 Sep 2015 12:19:58 +0200

libvisio (0.1.3-1) unstable; urgency=medium

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Sat, 25 Jul 2015 17:15:40 +0200

libvisio (0.1.1-3) unstable; urgency=medium

  * add missing libcppunit-dev and export TZ=CET to fix the test run

 -- Rene Engelhard <rene@debian.org>  Sat, 25 Apr 2015 10:36:01 +0200

libvisio (0.1.1-2) unstable; urgency=medium

  * upload to unstable

 -- Rene Engelhard <rene@debian.org>  Sun, 05 Apr 2015 19:57:55 +0200

libvisio (0.1.1-1) experimental; urgency=medium

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Fri, 02 Jan 2015 17:31:15 +0100

libvisio (0.1.0-2) unstable; urgency=low

  * upload to unstable

  * fix debian/copyright (MPL-2.0 only) 

 -- Rene Engelhard <rene@debian.org>  Fri, 08 Aug 2014 00:22:59 +0200

libvisio (0.1.0-1) experimental; urgency=low

  * New upstream release

  * use dh-autoreconf (closes: #748780)

 -- Rene Engelhard <rene@debian.org>  Fri, 23 May 2014 21:24:14 +0200

libvisio (0.0.31-1) unstable; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Wed, 28 Aug 2013 11:24:25 +0200

libvisio (0.0.30-1) unstable; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Wed, 10 Jul 2013 20:17:28 +0200

libvisio (0.0.28-1) unstable; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Fri, 07 Jun 2013 00:34:12 +0200

libvisio (0.0.27-1) unstable; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Wed, 22 May 2013 20:05:13 +0200

libvisio (0.0.26-3) unstable; urgency=low

  * upload to unstable 

 -- Rene Engelhard <rene@debian.org>  Mon, 22 Apr 2013 20:44:19 +0200

libvisio (0.0.26-2) experimental; urgency=low

  * add libicu-dev build-dep, sigh 

 -- Rene Engelhard <rene@debian.org>  Mon, 22 Apr 2013 20:34:19 +0200

libvisio (0.0.26-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Mon, 22 Apr 2013 19:26:38 +0200

libvisio (0.0.25-1) experimental; urgency=low

  * New upstream release

  * apply patches from Benjamin Drung:
    - enable hardening
    - set docdir instead of fixing the installation directory afterwards
    - lintian fixes, Standards-Version: 3.9.4 (no changes needed)
    - add homepage field.
    - Add watch file.
    - run wrap-and-sort
  * --disable-silent-rules

 -- Rene Engelhard <rene@debian.org>  Wed, 27 Feb 2013 21:33:30 +0100

libvisio (0.0.24-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Tue, 18 Dec 2012 17:11:24 +0100

libvisio (0.0.23-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Tue, 04 Dec 2012 23:33:43 +0100

libvisio (0.0.22-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Thu, 29 Nov 2012 18:53:47 +0100

libvisio (0.0.21-2) experimental; urgency=low

  * use --disable-werror... 

 -- Rene Engelhard <rene@debian.org>  Fri, 23 Nov 2012 10:49:55 +0100

libvisio (0.0.21-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Wed, 21 Nov 2012 22:48:18 +0100

libvisio (0.0.20-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Mon, 05 Nov 2012 17:47:42 +0100

libvisio (0.0.19-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Thu, 09 Aug 2012 14:35:59 +0200

libvisio (0.0.18-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Tue, 10 Jul 2012 22:34:55 +0200

libvisio (0.0.17-1) unstable; urgency=medium

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Thu, 31 May 2012 22:54:01 +0200

libvisio (0.0.16-1) unstable; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Mon, 16 Apr 2012 15:25:44 +0200

libvisio (0.0.15-2) unstable; urgency=low

  * upload to unstable 

 -- Rene Engelhard <rene@debian.org>  Wed, 28 Mar 2012 17:26:34 +0200

libvisio (0.0.15-1) experimental; urgency=low

  * New upstream release
  * work around  upstream ABI change in function only used by vsd2xhtml
    and not bumping the SONAME. (add Breaks and bump .shlibs)... 

 -- Rene Engelhard <rene@debian.org>  Mon, 20 Feb 2012 20:23:27 +0000

libvisio (0.0.14-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Fri, 20 Jan 2012 12:14:48 +0000

libvisio (0.0.13-1) experimental; urgency=low

  * New upstream release
  * remove "You can find it being used in libreoffice." from descriptions,
    gets confusing 

 -- Rene Engelhard <rene@debian.org>  Tue, 17 Jan 2012 11:44:23 +0000

libvisio (0.0.12-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Mon, 09 Jan 2012 18:11:08 +0000

libvisio (0.0.11-2) experimental; urgency=low

  * oops - add missing build-deps:
    pkg-config, doxygenm, libboost-dev

 -- Rene Engelhard <rene@debian.org>  Sun, 25 Dec 2011 18:31:40 +0100

libvisio (0.0.11-1) experimental; urgency=low

  * Initial release

 -- Rene Engelhard <rene@debian.org>  Sat, 24 Dec 2011 13:50:11 +0000
